import axios from 'axios';
import { Action } from 'redux'
import { ThunkAction } from 'redux-thunk'

import { HelloState, REQUEST, SUCCESS, FAILED, History, HelloActionTypes } from './types'
import { AppState } from '../store';

const INITIAL_STATE: HelloState = {
  message: "",
  loading: false,
  history: [],
  error: undefined
};

export default function reducer(
  state = INITIAL_STATE,
  action: HelloActionTypes
): HelloState {
  switch (action.type) {
    case REQUEST:
      return { ...state, loading: true };
    case SUCCESS:
      return { 
        ...state, 
        loading: false, 
        message: action.payload.response, 
        history: [...state.history, action.payload],
      };
    case FAILED:
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
}

function requestHello(): HelloActionTypes {
  return { type: REQUEST };
}

function successHello(payload: History): HelloActionTypes {
  return { type: SUCCESS, payload };
}

function failedHello(payload: string): HelloActionTypes {
  return { type: FAILED, payload };
}

export function fetchMessage(
  message: string
): ThunkAction<void, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    dispatch(requestHello());
    try {
      const res = await axios.get(`http://68.183.230.47:8000/hello/${message}`);
      const result = res.data._msg;

      dispatch(successHello({ request: message, response: result}));
    } catch (error) {
      dispatch(failedHello(error));
    }
  }
}

export const REQUEST = "hello/REQUEST";
export const SUCCESS = "hello/SUCCESS";
export const FAILED = "hello/FAILED";

export interface HelloState {
    message: string
    loading: boolean
    history: History[]
    error?: string
}

export interface History {
    request: string
    response: string
}

interface RequestHelloAction {
    type: typeof REQUEST
}

interface SuccessHelloAction {
    type: typeof SUCCESS
    payload: History
}

interface FailedHelloAction {
    type: typeof FAILED
    payload: string
}

export type HelloActionTypes = RequestHelloAction | SuccessHelloAction | FailedHelloAction;
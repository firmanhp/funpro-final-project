import axios from 'axios';
import { Action } from 'redux'
import { ThunkAction } from 'redux-thunk'

import { CalculatorState, SETFILTER, REQUEST, SUCCESS, FAILED, Filter, History, CalculatorActionTypes } from './types'
import { AppState } from '../store';

const INITIAL_STATE: CalculatorState = {
  result: {
    downPayment: 0,
    installment: 0,
    interestRate: 0,
    repayment: 0,
    car: {
      brand: "",
      year: 0,
      model: "",
      price: 0,
      cc: 0
    },
    downPaymentRate: 0
  },
  loading: false,
  history: [],
  filter: {
      year: 0,
      price: 0,
      cc: 0
    },
  error: undefined
};

export default function reducer(
  state = INITIAL_STATE,
  action: CalculatorActionTypes
): CalculatorState {
  switch (action.type) {
    case REQUEST:
      return { ...state, loading: true };
    case SUCCESS:
      return { 
        ...state, 
        error: undefined,
        loading: false, 
        result: action.payload.response, 
        history: [...state.history, action.payload],
      };
    case FAILED:
      return { ...state, loading: false, error: action.payload };
    case SETFILTER:
      return { ...state, filter: action.payload };
    default:
      return state;
  }
}

function setFilter(payload: Filter): CalculatorActionTypes {
  return { type: SETFILTER, payload };
}

function requestCalculator(): CalculatorActionTypes {
  return { type: REQUEST };
}

function successCalculator(payload: History): CalculatorActionTypes {
  return { type: SUCCESS, payload };
}

function failedCalculator(payload: string): CalculatorActionTypes {
  return { type: FAILED, payload };
}

export function calculate(
  installment: number,
  maturity: number,
  bankName: string
): ThunkAction<void, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    dispatch(requestCalculator());
    try {
      const res = await axios.get(`http://68.183.230.47:8000/calculate?installment=${installment}&maturity=${maturity}&bankName=${bankName}`);
      const result = res.data;

      if (result === null) {
        dispatch(failedCalculator("Tidak terdapat mobil tersedia dengan pengaturan cicilan tersebut. Coba pengaturan lain."));
      } else {
        dispatch(successCalculator({ request: {installment, maturity, bankName}, response: result}));
      }
    } catch (error) {
      dispatch(failedCalculator(error));
    }
  }
}

export function filter(
  year: number,
  price: number,
  cc: number
): ThunkAction<void, AppState, null, Action<string>> {
  return async (dispatch, getState) => {
    dispatch(setFilter({ year,price,cc }));
  }
}

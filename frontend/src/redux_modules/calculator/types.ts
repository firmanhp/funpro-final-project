export const REQUEST = "calculator/REQUEST";
export const SUCCESS = "calculator/SUCCESS";
export const FAILED = "calculator/FAILED";
export const SETFILTER = "calculator/FILTER";

export interface CalculatorState {
    result: CalculatorResponse
    loading: boolean
    history: History[]
    error?: string
    filter: Filter
}

export interface History {
    request: CalculatorRequest
    response: CalculatorResponse
}

export interface CalculatorRequest {
    installment: number
    maturity: number
    bankName: string
}

export interface CalculatorResponse {
    downPayment: number
    installment: number
    interestRate: number
    repayment: number
    car: Car
    downPaymentRate: number
}

export interface Car {
    brand: string
    year: number
    model: string
    price: number
    cc: number
}

export interface Filter {
    year: number
    price: number
    cc: number
}

interface SetFilterAction {
    type: typeof SETFILTER
    payload: Filter
}

interface RequestCalculatorAction {
    type: typeof REQUEST
}

interface SuccessCalculatorAction {
    type: typeof SUCCESS
    payload: History
}

interface FailedCalculatorAction {
    type: typeof FAILED
    payload: string
}

export type CalculatorActionTypes = RequestCalculatorAction | SuccessCalculatorAction | FailedCalculatorAction | SetFilterAction;
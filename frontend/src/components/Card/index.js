import React from 'react';
import { Default, Notes } from "./style"

export const Card = ({ children, style }) => (
  <div style={style==="Notes" ? Notes : Default}>
      { children }
  </div>
);
import React from 'react';
import { Card } from './index';

export default {
  title: 'Card',
};

export const Default = () => <Card>This is a card</Card>;

export const Notes = () => <Card style={"Notes"}>This is a note</Card>;
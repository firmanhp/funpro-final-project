export const Default = {
    background: "#FFFFFF",
    border: "1px solid #E0E0E0",
    borderRadius: "8px",
    padding: "2rem",
    paddingBottom: "1.25rem",
    width: "40vw",
    margin: "5rem 1rem",
}

export const Notes = {
    background: "rgba(86, 204, 242, 0.15)",
    border: "none",
    borderRadius: "8px",
    padding: "0.25rem 1rem",
    width: "20vw",
    margin: "5rem 1rem",
    height: "auto",
}
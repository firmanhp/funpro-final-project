import React, { useState } from 'react';
import { TextField } from './index';

export default {
  title: 'TextField',
};

export const Idle = () => {
    return (
        <TextField>
            Lable
        </TextField>
    );
}

export const Filled = () => {
    const [input, setInput] = useState(
        'Lorem ipsum dolor'
    );
    const handleChange = event => setInput(event.target.value);

    return (
        <TextField value={input} onChangeInput={handleChange}>
            Lable
        </TextField>
    );
}

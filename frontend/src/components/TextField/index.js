import React from 'react';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import { makeStyles } from '@material-ui/core/styles';


export const TextField = ({ value, onChangeInput, children, type }) => {
    const useStyles = makeStyles(theme => ({
        container: {
          display: 'flex',
          flexWrap: 'wrap',
        },
        formControl: {
          margin: "0.5rem 0",
          width: '100%',
        },
        componentOutlined: {
            width: '100%',
        }
      }));

    const classes = useStyles();
    const labelRef = React.useRef(null);

    return (
        <FormControl className={classes.formControl} variant="outlined">
            <InputLabel ref={labelRef} htmlFor="component-outlined">
                { children }
            </InputLabel>
            <OutlinedInput
                type={type}
                width={"100%"}
                id="component-outlined"
                value={value}
                onChange={onChangeInput}
                labelWidth={children}
                inputProps={{
                    size: 100
                }}
            />
        </FormControl>
    );
}
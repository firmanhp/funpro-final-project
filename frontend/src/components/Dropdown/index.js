import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

export const Dropdown = ({ value, onChangeInput, children, array }) => {

  const useStyles = makeStyles(theme => ({
    formControl: {
      margin: "0.5rem 0",
      width: '100%',
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  }));

  const classes = useStyles();
  const labelRef = React.useRef(null);

  const [labelWidth, setLabelWidth] = React.useState(0);
  React.useEffect(() => {
    setLabelWidth(labelRef.current.offsetWidth);
  }, []);

  return (
    <FormControl variant="outlined" className={classes.formControl}>
      <InputLabel ref={labelRef} id="demo-simple-select-outlined-label">
        {children}
      </InputLabel>
      <Select
        labelId="demo-simple-select-outlined-label"
        id="demo-simple-select-outlined"
        value={value}
        onChange={e => onChangeInput(e.target.value)}
        labelWidth={labelWidth}
      >
        {
          array.map((item) =>
            <MenuItem value={item.name}>{item.name}</MenuItem>
          )
        }
      </Select>
    </FormControl>
  );
}
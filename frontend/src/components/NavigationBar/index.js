import React from 'react';
import { NavLink as Link } from "react-router-dom";
import { root, navbar, link } from './style';

export const NavigationBar = () => (
    <div style={root}>
        <div style={navbar}>
            <h4>CicilMobilPas</h4>
            <div>
                <Link to="/beranda" style={link.root} activeClassName={link.active}>Beranda</Link>
                <Link to="/riwayat" style={link.root} activeClassName={link.active}>Riwayat Pencarian Mobil</Link>
            </div>
        </div>
    </div>
);

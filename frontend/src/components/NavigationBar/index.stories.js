import React from 'react';
import { BrowserRouter as Router } from "react-router-dom";

import { NavigationBar } from './index';

export default {
  title: 'NavigationBar',
};

export const Default = () => (
    <Router>
        <NavigationBar/>
    </Router>
);
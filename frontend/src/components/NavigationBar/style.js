export const root = {
    backgroundColor: "#2D9CDB",
    display: "fixed",
    top: "0px",
    color: "white",
}

export const navbar = {
    width: "80vw",
    display: "flex",
    flexGrow: 1,
    justifyContent: "space-between",
    flexDirection: "row",
    alignItems: "center",
    margin: "auto",
}

export const link = {
    root: {
        textDecoration: "none",
        margin: "0 1rem",
        color: "white",
    },
    active: {
        backgroundColor: "white",
        border: "1px solid white",
    }
}
export const Primary = {
    padding: "0 1.5rem",
    border: "none",
    borderRadius: "4px",
    cursor: "pointer",
    backgroundColor: "#2D9CDB",
    margin: "0.5rem 0",
}

export const Secondary = {
    padding: "0 1.5rem",
    border: "2px solid #2D9CDB",
    borderRadius: "4px",
    cursor: "pointer",
    color: "#2D9CDB",
    backgroundColor: "white",
    margin: "0.5rem 0",
}
import React, { useState } from 'react';
import { Button } from './index';

export default {
  title: 'Button',
};

export const Primary = () => <Button>Hello Button</Button>;

export const Secondary = () => <Button style={"Secondary"}>Hello Button</Button>;
import React from 'react';
import { Primary, Secondary } from "./style";
import { ButtonText } from "../../components/Typography";

export const Button = ({ handleClick, children, type, style }) => (
  <button type={type} onClick={handleClick} style={style==="Secondary" ? Secondary : Primary}>
    <ButtonText>{children}</ButtonText>
  </button>
);
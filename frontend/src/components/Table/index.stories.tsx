import * as React from 'react';
import { storiesOf } from '@storybook/react';
import { EnhancedTable } from './index';

const stories = storiesOf('Components', module);

function createData(
  merek: string,
  model: string,
  tahun: number,
  harga: number,
  cc: number,
): any {
  return { merek, model, tahun, harga, cc };
}

const rows = [
  createData('Audi', '3.0 Multutronic quatro', 2005, 500000000, 3000),
  createData('BMW', 'M6', 2019, 1500000000, 3000),
];

stories.add(
  'EnhancedTable',
  () => <EnhancedTable data={rows}/>,
  { info: { inline: true } }
);
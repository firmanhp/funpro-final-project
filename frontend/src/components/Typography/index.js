import styled from 'styled-components';

// Heading 1
export const H1 = styled.h1`
    /* Heading / H1 */
    font-family: 'Roboto', sans-serif;
    font-style: normal;
    font-weight: bold;
    font-size: 32px;
    line-height: 55px;

    /* identical to box height, or 172% */
    letter-spacing: 0.25px;

    /* Gray 1 */
    color: #333333;
`

// Heading 2
export const H2 = styled.h2`
    /* Heading / H2 */
    font-family: 'Roboto', sans-serif;
    font-style: normal;
    font-weight: bold;
    font-size: 24px;
    line-height: 55px;

    /* identical to box height, or 172% */
    letter-spacing: 0.25px;
    text-align: center;

    /* Gray 1 */
    color: #333333;
`

// Subtitle 
export const SubtitleText = styled.p`
    /* Paragraph / Subtitle 1 */
    font-family: 'Roboto', sans-serif;
    font-style: normal;
    font-weight: 500;
    font-size: 16px;
    line-height: 26px;

    /* or 162% */
    letter-spacing: 0.15px;

    /* Gray 1 */
    color: #333333;
`

// Body Text
export const BodyText = styled.p`
    /* Paragraph / Body 2 */
    font-family: 'Lato', sans-serif;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 23px;

    /* or 164% */
    letter-spacing: 0.25px;

    /* Gray 1 */
    color: #333333;
`

// Button Text
export const ButtonText = styled.p`
    /* Other / Button */
    font-family: 'Roboto', sans-serif;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;

    /* identical to box height, or 164% */
    align-items: center;
    text-align: center;
    letter-spacing: 1.25px;
    text-transform: uppercase;

    color: #FFFFFF;
`
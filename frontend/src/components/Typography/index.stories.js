import React from 'react';

import { H1, SubtitleText, BodyText, ButtonText } from './index';

export default {
  title: 'Typography',
};

export const Heading = () => <H1>This is a Heading</H1>;

export const Subtitle = () => <SubtitleText>This is a Subtitle</SubtitleText>;

export const Body = () => <BodyText>This is a BodyText</BodyText>;

export const Button = () => <ButtonText>This is a ButtonText</ButtonText>;

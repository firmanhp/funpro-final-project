import React, { Component } from "react";
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from "react-redux";
import { fetchMessage } from '../../redux_modules/hello';
import { AppState } from '../../redux_modules/store';
import { HelloActionTypes } from '../../redux_modules/hello/types';

import { TextField } from "../../components/TextField";
import { Button } from "../../components/Button";
import { Card } from "../../components/Card";
import { H1 } from "../../components/Typography";

const mapStateToProps = (state: AppState) => ({
  hello: state.hello,
});

const mapDispatchToProps = (dispatch: Dispatch<HelloActionTypes>) =>
  bindActionCreators(
    {
      fetchMessage
    },
    dispatch
  );

type Props = ReturnType<typeof mapStateToProps> &
ReturnType<typeof mapDispatchToProps>

interface State {
  message: string
}

class Hello extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { message: "Hai" };

    this.onInputChange = this.onInputChange.bind(this);
    this.onSubmitForm = this.onSubmitForm.bind(this);
  }

  onInputChange(event: React.FormEvent<HTMLInputElement>) {
    this.setState({ message: event.currentTarget.value});
  }

  async onSubmitForm(event: React.FormEvent) {
    event.preventDefault();
    this.props.fetchMessage(this.state.message);
  }

  render() {
    return (
      <React.Fragment>
        <Card style={"Default"}>
          <H1>Welcome</H1>
          <form
            onSubmit={this.onSubmitForm}
          >
            <TextField type={"text"} value={this.state.message} onChangeInput={this.onInputChange}>Hello</TextField>
            <br/>
            <Button handleClick={""} style={"Primary"} type={"submit"}>Submit brre</Button>
          </form>
          {!this.props.hello.loading && <div>{this.props.hello.message}</div>}
          {!!this.props.hello.loading && <div>Loading</div>}
        </Card>
      </React.Fragment>
    )
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Hello);

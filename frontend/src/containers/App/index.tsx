import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";

import './style.css';
import Hello from '../Hello';
import { NavigationBar } from "../../components/NavigationBar";
import { EnhancedTable } from "../../components/Table";

import Beranda from "../../containers/Beranda";
import Riwayat from "../../containers/Riwayat";


const App: React.FC = () => {
  return (
    <Router>
      <NavigationBar/>
      <Route path="/" exact component={Beranda} />
      <Route path="/beranda" exact component={Beranda} />
      <Route path="/riwayat" exact component={Riwayat} />
    </Router>
  );
}

export default App;

import { CSSProperties } from "react";

export const RiwayatLayout: CSSProperties = {
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "center",
}
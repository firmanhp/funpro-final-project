import React, { useState } from 'react';
import { connect, useDispatch } from "react-redux";

import { AppState } from '../../redux_modules/store';
import { TextField } from "../../components/TextField";
import { Button } from "../../components/Button";
import { EnhancedTable } from "../../components/Table";
import { H2 } from "../../components/Typography";
import { RiwayatLayout } from './style';
import { calculate } from '../../redux_modules/calculator';

const mapStateToProps = (state: AppState) => ({
    history: state.calculator.history,
});
const mapDispatchToProps = { calculate };

type Props = ReturnType<typeof mapStateToProps> 

const Riwayat: React.FC<Props> = (props: Props) => {
	const [year, setYear] = useState(
        2019
    );
    const [price, setPrice] = useState(
        
    );
    const [capacity, setCapacity] = useState(
        
    );
    const dispatch = useDispatch();
    const handleChangeYear = (event: React.FormEvent<HTMLInputElement>) => {
        let target = event.currentTarget as HTMLInputElement;
        setYear(parseInt(target.value));
    }
    const handleChangePrice = (event: React.FormEvent<HTMLInputElement>) => {
        let target = event.currentTarget as HTMLInputElement;
        setPrice(parseInt(target.value));
    }
    const handleChangeCapacity = (event: React.FormEvent<HTMLInputElement>) => {
        let target = event.currentTarget as HTMLInputElement;
        setCapacity(parseInt(target.value));
    }

    const onSubmitForm = async (event: React.FormEvent) => {
        event.preventDefault();
        
    }

    const createTableData = (history: any): any => {
        return {
            merek: history.response.car.brand,
            model: history.response.car.model,
            tahun: history.response.car.year,
            harga: history.response.car.price,
            cc: history.response.car.cc
        }
    }
    return (
        <React.Fragment>
            <H2 align="center">Riwayat Pencarian Mobil</H2>

            <EnhancedTable data={props.history.map(createTableData)}/>

        </React.Fragment>
    )
};

export default connect<any,any,any,any>(
    mapStateToProps,
    mapDispatchToProps
)(Riwayat);

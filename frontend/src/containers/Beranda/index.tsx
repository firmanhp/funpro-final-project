import React, { useState, useEffect } from 'react';
import { connect, useDispatch } from "react-redux";
import axios from 'axios';

import { calculate } from '../../redux_modules/calculator';
import { AppState } from '../../redux_modules/store';

import { Dropdown } from "../../components/Dropdown";
import { TextField } from "../../components/TextField";
import { Button } from "../../components/Button";
import { Card } from "../../components/Card";
import { H1, SubtitleText, BodyText } from "../../components/Typography";
import { BerandaLayout } from './style';

const mapStateToProps = (state: AppState) => ({
  calculator: state.calculator,
});

const mapDispatchToProps = { calculate };

type Props = ReturnType<typeof mapStateToProps> 

const Beranda: React.FC<Props> = (props: Props) => {
    const [installment, setInstallment] = useState(
        5000000
    ); const [maturity, setMaturity] = useState(
        60
    ); const [bank, setBank] = useState(
        'Mandiri Tunas Finance KPM'
    ); const [listOfBanks, setListOfBanks] = useState(
        []
    );

    const dispatch = useDispatch();

    useEffect(() => {
        const fetchData = async () => {
          const result = await axios(
            'http://68.183.230.47:8000/bank',
          );
          setListOfBanks(result.data);
        };
        fetchData();
      }, []);

    const handleChangeInstallment = (event: React.FormEvent<HTMLInputElement>) => {
        let target = event.currentTarget as HTMLInputElement;
        setInstallment(parseInt(target.value));
    }
    const handleChangeMaturity = (event: React.FormEvent<HTMLInputElement>) => {
        let target = event.currentTarget as HTMLInputElement;
        setMaturity(parseInt(target.value));
    }

    const handleChangeBank = (bankName: string) => {
        setBank(bankName);
    }

    const onSubmitForm = async (event: React.FormEvent) => {
        event.preventDefault();
        dispatch(calculate(installment, maturity, bank));
    }

    return (
        <React.Fragment>
            <div style={BerandaLayout}>
                <div>
                    <Card style={"Default"}>
                        <H1>Cari Mobil Berdasarkan Kemampuan Kredit</H1>
                        <form onSubmit={onSubmitForm}>
                        <TextField type={"number"} value={installment} onChangeInput={handleChangeInstallment}>Angsuran Bulanan</TextField>
                        <TextField type={"number"} value={maturity} onChangeInput={handleChangeMaturity}>Tenor</TextField>
                        <Dropdown array={listOfBanks} value={bank} onChangeInput={handleChangeBank}>Perusahaan Pengajuan Kredit</Dropdown>
                        <br/>
                        <Button handleClick={""} style={"Primary"} type={"submit"}>Cari Mobil</Button>
                        </form>
                    </Card>
                    <Card style={"Default"}>
                        <H1>Hasil Pencarian Mobil</H1>
                        {!!props.calculator.error && <BodyText>{props.calculator.error}</BodyText>}
                        {!props.calculator.error && 
                            (
                                <React.Fragment>
                                    <SubtitleText>{props.calculator.result.car.brand}</SubtitleText>
                                    <BodyText>Tahun : {props.calculator.result.car.year}</BodyText>
                                    <BodyText>Harga Mobil : Rp.{props.calculator.result.car.price}</BodyText>
                                    <BodyText>Kapasitas CC : {props.calculator.result.car.cc}cc</BodyText>
                                    <hr/>
                                    <BodyText>Bunga Cicilan : {props.calculator.result.interestRate}%</BodyText>
                                    <BodyText>Persentase DP : {props.calculator.result.downPaymentRate}%</BodyText>
                                    <BodyText>Uang DP : Rp.{props.calculator.result.downPayment}</BodyText>
                                    <BodyText>Angsuran Bulanan : Rp.{props.calculator.result.installment}</BodyText>
                                    <BodyText>Harga yang Dibayarkan : Rp.{props.calculator.result.repayment}</BodyText>

                                </React.Fragment>
                            )
                        }
                    </Card>
                </div>
                <Card style={"Notes"}>
                    <SubtitleText>Tips dalam melakukan kredit mobil</SubtitleText>
                    <BodyText>Sumber: <a href="https://www.seva.id/blog/6-tips-mendapatkan-cicilan-ringan-saat-beli-mobil-baru/">seva.id</a></BodyText>
                    <BodyText>1. Pilih jenis mobil sesuai kebutuhan</BodyText>
                    <BodyText>2. Perbesar uang muka</BodyText>
                    <BodyText>3. Pilih jangka waktu cicilan terpanjang</BodyText>
                    <BodyText>4. Cari asuransi sendiri</BodyText>
                    <BodyText>5. Beli di waktu yang tepat</BodyText>
                </Card>
            </div>
        </React.Fragment>
    )
};

export default connect<any,any,any,any>(
  mapStateToProps,
  mapDispatchToProps
)(Beranda);
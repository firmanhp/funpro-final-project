import { CSSProperties } from "react";

export const BerandaLayout: CSSProperties = {
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "center",
}
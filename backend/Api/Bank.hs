{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

module Api.Bank(ApiSpec, server) where

import Control.Monad.IO.Class

import Servant
import Data.Text (Text)
import Api.Common.Bank

type ApiSpec =
  -- GET /bank returns a list of available banks
  "bank"  :> Get '[JSON] [Bank]

server :: Server ApiSpec
server = getBanks
  where
    getBanks :: Handler [Bank]
    getBanks = liftIO $ loadBankCSV 
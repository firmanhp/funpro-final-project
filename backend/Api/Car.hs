{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

module Api.Car(ApiSpec, server) where

import Control.Monad.IO.Class

import Servant
import Data.Text (Text)
import Api.Common.Car

type ApiSpec =
  -- GET /car?price={int}  returns a list of cars below price as JSON, sorted by highest price
  "car" :> QueryParam "price" Int :> Get '[JSON] [Car]

server :: Server ApiSpec
server = getCar
  where
    getCar :: Maybe Int -> Handler [Car]
    getCar Nothing = liftIO loadCarCSV
    getCar (Just price) = liftIO $ fmap (`filterCarsBelowPrice` price) loadCarCSV
{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE TypeOperators     #-}

module Api.Greet(ApiSpec, server) where

import Servant
import Data.Text
import Api.Common.Greet


type ApiSpec =
  -- POST /greet with a Greet as JSON in the request body,
  --             returns a Greet as JSON
       "greet" :> ReqBody '[JSON] Greet :> Post '[JSON] Greet
  
  -- DELETE /greet/:greetid
  :<|> "greet" :> Capture "greetid" Text :> Delete '[JSON] NoContent

server :: Server ApiSpec
server = postGreetH :<|> deleteGreetH
  where postGreetH greet = return greet
        deleteGreetH _ = return NoContent

{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE DuplicateRecordFields     #-}

module Api.Common.Calculator(CalculatorResult (..), InstallmentConfig (..),
  getMaxLoanAmount, getDownPayment, getCarInstallment, getRepayment, getInstallmentConfig
) where

import Api.Common.Car (Car (..))
import Api.Common.Bank
import Data.Aeson
import GHC.Generics

data CalculatorResult = CalculatorResult {
    interestRate :: Float,
    downPaymentRate :: Float,
    downPayment :: Int,
    carInstallment :: Int,
    repayment :: Int,
    car :: Car }
  deriving (Generic, Eq, Show)

instance FromJSON CalculatorResult
instance ToJSON CalculatorResult

data InstallmentConfig = InstallmentConfig {
  bank :: Bank,
  installment :: Int,
  maturity :: Int,
  interestRate :: Float,
  maxLoanAmount :: Int }
  deriving (Generic, Eq, Show)

getMaxLoanAmount :: Int -> Int -> Float -> Int
getMaxLoanAmount installment maturity interestRate = round (money * (100 - interestRate) / 100)
  where money = fromIntegral installment * fromIntegral maturity

getDownPayment :: Bank -> Car -> Int
getDownPayment bank car =
  round (fromIntegral (price car) * downPaymentRate (bank :: Bank) / 100)

getCarInstallment :: InstallmentConfig -> Int -> Car -> Int
getCarInstallment installmentConfig downPayment car =
  round (fromIntegral (price car - downPayment) / fromIntegral (maturity installmentConfig))

getRepayment :: InstallmentConfig -> Car -> Int
getRepayment installmentConfig car =
  round (fromIntegral (price car) * (100 + interestRate (installmentConfig :: InstallmentConfig)) / 100 )

getInstallmentConfig :: Bank -> Int -> Int -> Maybe InstallmentConfig
getInstallmentConfig bank installment maturity =
  case getInterestRate bank maturity of
    Nothing -> Nothing
    (Just interestRate) -> Just InstallmentConfig {
      bank = bank,
      installment = installment,
      maturity = maturity,
      interestRate = interestRate,
      maxLoanAmount = getMaxLoanAmount installment maturity interestRate
    }

{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

module Api.Common.Car(Car (..), loadCarCSV, compareByPrice, filterCarsBelowPrice) where

import Data.Text (Text (..))
import Data.Aeson
import Data.Csv
import GHC.Generics
import Data.Text.Encoding
import Data.Vector (Vector (..), toList)

-- base
import Control.Exception (IOException)
import qualified Control.Exception as Exception
import qualified Data.Foldable as Foldable

-- bytestring
import Data.ByteString.Lazy (ByteString)
import qualified Data.ByteString.Lazy as ByteString

import Data.List (filter, sortBy, reverse)

data Car = Car {
    brand :: Text,
    model :: Text,
    year :: Int,
    cc :: Int,
    price :: Int }
  deriving (Generic, Eq, Show)

instance FromJSON Car
instance ToJSON Car
instance FromNamedRecord Car

compareByPrice :: Car -> Car -> Ordering
compareByPrice car1 car2 = compare (price car1) (price car2)

decodeCars :: ByteString -> Either String (Vector Car)
decodeCars = fmap snd . decodeByName

decodeCarsFromFile :: FilePath -> IO (Either String (Vector Car))
decodeCarsFromFile filePath = catchShowIO (ByteString.readFile filePath)
  >>= return . either Left decodeCars

catchShowIO :: IO a -> IO (Either String a)
catchShowIO action = fmap Right action `Exception.catch` handleIOException
  where handleIOException :: IOException -> IO (Either String a)
        handleIOException = return . Left . show

processCars :: Either String (Vector Car) -> [Car]
processCars (Left _) = [] -- Car {brand=pack x, model="x", year=1, cc=2, price=3} for debug
processCars (Right x) = toList x

loadCarCSV :: IO [Car]
loadCarCSV = do
  results <- decodeCarsFromFile "data/data_mobil_clean_new.csv"
  return (processCars results)
  
filterCarsBelowPrice :: [Car] -> Int -> [Car]
filterCarsBelowPrice cars priceThreshold = sortBy (flip compareByPrice)
  $ filter (\c -> price c <= priceThreshold) cars
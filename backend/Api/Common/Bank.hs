{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

module Api.Common.Bank(Bank (..), loadBankCSV, getInterestRate) where

import Data.Text
import Data.Aeson
import Data.Csv
import GHC.Generics
import Data.Text.Encoding
import Data.Vector

-- base
import Control.Exception (IOException)
import qualified Control.Exception as Exception
import qualified Data.Foldable as Foldable

-- bytestring
import Data.ByteString.Lazy (ByteString)
import qualified Data.ByteString.Lazy as ByteString

data Bank = Bank {
    name :: String,
    downPaymentRate :: Float,
    one :: Maybe Float,
    two :: Maybe Float,
    three :: Maybe Float,
    four :: Maybe Float,
    five :: Maybe Float }
  deriving (Generic, Eq, Show)

instance FromJSON Bank
instance ToJSON Bank
instance FromNamedRecord Bank

decodeBanks :: ByteString -> Either String (Vector Bank)
decodeBanks = fmap snd . decodeByName

decodeBanksFromFile :: FilePath -> IO (Either String (Vector Bank))
decodeBanksFromFile filePath = catchShowIO (ByteString.readFile filePath)
  >>= return . either Left decodeBanks

catchShowIO :: IO a -> IO (Either String a)
catchShowIO action = fmap Right action `Exception.catch` handleIOException
  where handleIOException :: IOException -> IO (Either String a)
        handleIOException = return . Left . show

processBanks :: Either String (Vector Bank) -> [Bank]
processBanks (Left _) = [] -- Bank {brand=pack x, model="x", year=1, cc=2, price=3} for debug
processBanks (Right x) = toList x

loadBankCSV :: IO [Bank]
loadBankCSV = do
  results <- decodeBanksFromFile "data/data_bank2.csv"
  return (processBanks results)

getInterestRate :: Bank -> Int -> Maybe Float
getInterestRate bank maturity
  | maturity == 1 = one bank
  | maturity == 2 = two bank
  | maturity == 3 = three bank
  | maturity == 4 = four bank
  | otherwise = five bank
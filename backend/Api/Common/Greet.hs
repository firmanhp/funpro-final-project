{-# LANGUAGE DeriveGeneric     #-}

module Api.Common.Greet(Greet (..)) where

import Data.Text
import Data.Aeson
import GHC.Generics


newtype Greet = Greet { _msg :: Text }
  deriving (Generic, Show)

instance FromJSON Greet
instance ToJSON Greet

{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE DuplicateRecordFields     #-}

module Api.Calculator(ApiSpec, server) where

import Control.Monad.IO.Class

import Servant
import Data.Text (Text)
import Api.Common.Calculator (CalculatorResult (..), InstallmentConfig (..),
    getMaxLoanAmount, getDownPayment, getCarInstallment, getRepayment, getInstallmentConfig
  )
import Api.Common.Bank (Bank (..), getInterestRate, loadBankCSV)
import Api.Common.Car (Car (..), loadCarCSV, filterCarsBelowPrice)
import Data.List (filter, sortBy, reverse)

type ApiSpec =
  -- GET /calculate?installment={int}&maturity={int}&bankName={string} 
  --       returns a car below price as JSON
  "calculate" :> QueryParam "installment" Int 
              :> QueryParam "maturity" Int 
              :> QueryParam "bankName" String 
              :> Get '[JSON] (Maybe CalculatorResult)
 
getBankByName :: [Bank] -> String -> Bank
getBankByName banks bankName = head (filter (\c -> name c == bankName) banks)

getSelectedBank :: String -> IO Bank
getSelectedBank name = loadBankCSV >>= ( \a -> return (getBankByName a name))

getRecommendedCar :: InstallmentConfig -> IO (Maybe Car)
getRecommendedCar installmentConfig =
  fmap (`filterCarsBelowPrice` maxLoanAmount installmentConfig) loadCarCSV 
  >>= ( return . (\l -> if null l then Nothing else Just (head l)) )

server :: Server ApiSpec
server = getBank
  where
    getBank :: Maybe Int -> Maybe Int -> Maybe String -> Handler (Maybe CalculatorResult)
    getBank (Just installment) (Just maturity) (Just bankName) = liftIO 
      $ do
          bank <- getSelectedBank bankName
          let maybeInstallmentConfig = getInstallmentConfig bank installment maturity
          case maybeInstallmentConfig of
            Nothing -> return Nothing
            (Just installmentConfig) -> do
              maybeCar <- getRecommendedCar installmentConfig
              case maybeCar of
                Nothing -> return Nothing
                Just car -> do
                  let downPayment = getDownPayment bank car
                  let carInstallment = getCarInstallment installmentConfig downPayment car
                  let repayment = getRepayment installmentConfig car

                  return (Just (CalculatorResult {
                    interestRate = interestRate (installmentConfig :: InstallmentConfig),
                    downPaymentRate = downPaymentRate (bank :: Bank),
                    downPayment = downPayment,
                    carInstallment = carInstallment,
                    repayment = repayment,
                    car = car}))
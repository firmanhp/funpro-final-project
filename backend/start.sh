#!/bin/sh

if [ ! "$(ls -A ~/.cabal/)" ]; then
    echo "Cabal config folder (.cabal/) folder is empty, will run cabal new-update"
    cabal new-update --verbose=2
else
    echo "Cabal config folder is not empty, will skip update"
fi
cabal new-run


import re
def hasNumbers(inputString):
    return bool(re.search(r'\d', inputString))

def main():
    file = open("data_mobil.csv","r+")
    out = open("data_mobil_clean_new.csv","w+")
    sentence = (file.readlines())
    word = ''
    cubo = ''
    for line in sentence:
        words = line.split()
        new = []
        if line == "\n" or line == "":
            pass
        else:
            if (len(words) < 3 and not words[0].isdigit()):             
                cubo = line[:-1]
            elif (len(words) >= 3 and (("." not in words[-1]) or ("." not in words[-2])) and not words[-3].isdigit()):               
                cubo = line[:-1]
            else:
                words[-1]=(words[-1].replace('.',''))
                words[-2]=(words[-2].replace(',','.'))
                words[-2]=(words[-2].replace('.',''))
                merk = (" ".join(words[:-3]))
                new.append(cubo)
                new.append(merk)
                new.extend(words[-3:])
                line = ','.join(map(str, new))
                out.write(line + "\n")
    out.close()
    file.close()
if __name__== "__main__":
    main()
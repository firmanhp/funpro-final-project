{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE PolyKinds         #-}
{-# LANGUAGE TypeFamilies      #-}
{-# LANGUAGE TypeOperators     #-}

module Main where

import           Data.Proxy
import           Network.Wai.Handler.Warp
import           Network.Wai.Middleware.Cors
import           Servant

import qualified Api.Hello as HelloApi
import qualified Api.Greet as GreetApi
import qualified Api.Car as CarApi
import qualified Api.Calculator as CalculatorApi
import qualified Api.Bank as BankApi

-- API specification
type GlobalApi = HelloApi.ApiSpec
                :<|> GreetApi.ApiSpec
                :<|> CarApi.ApiSpec
                :<|> CalculatorApi.ApiSpec 
                :<|> BankApi.ApiSpec 
globalApi :: Proxy GlobalApi
globalApi = Proxy

-- Server-side handlers.
server :: Server GlobalApi
server = HelloApi.server
        :<|> GreetApi.server
        :<|> CarApi.server
        :<|> CalculatorApi.server
        :<|> BankApi.server

-- Turn the server into a WAI app. 'serve' is provided by servant,
-- more precisely by the Servant.Server module.
app :: Application
app = simpleCors $ serve globalApi server

-- Run the server.
-- 'run' comes from Network.Wai.Handler.Warp
runServer :: Port -> IO ()
runServer port = run port app

-- Put this all to work!
main :: IO ()
main = runServer 8000


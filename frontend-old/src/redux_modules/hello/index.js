import axios from 'axios';

const REQUEST = "hello/REQUEST";
const SUCCESS = "hello/SUCCESS";
const FAILED = "hello/FAILED";

const INITIAL_STATE = {
  message: "",
  loading: false,
  error: null
};

export default function reducer(state = INITIAL_STATE, action = {}) {
  console.log(action);
  switch(action.type) {
    case REQUEST:
      return { ...state, loading: true };
    case SUCCESS:
      return { ...state, loading: false, message: action.payload };
    case FAILED:
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
}

function requestHello() {
  return { type: REQUEST };
}

function successHello(payload) {
  return { type: SUCCESS, payload };
}

function failedHello(payload) {
  return { type: FAILED, payload };
}

export function fetchMessage(message) {
  return async (dispatch, getState) => {
    dispatch(requestHello());
    try {
      const res = await axios.get(`http://68.183.230.47:8000/hello/${message}`);
      const result = res.data._msg;

      dispatch(successHello(result));
    } catch (error) {
      dispatch(failedHello(error));
    }
  }
}
import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchMessage } from '../../redux_modules/hello';

class Hello extends Component {
  constructor(props) {
    super(props);
    this.state = { message: "Hai" };

    this.onInputChange = this.onInputChange.bind(this);
    this.onSubmitForm = this.onSubmitForm.bind(this);
  }

  onInputChange(event) {
    this.setState({ message: event.target.value});
  }

  async onSubmitForm(event) {
    event.preventDefault();
    this.props.fetchMessage(this.state.message);
  }

  render() {
    return (
      <div>
        <form
          onSubmit={this.onSubmitForm}
        >
          <input
            type="text"
            value={this.state.message}
            onChange={this.onInputChange}
          />
          <input type="submit" value="Submit" />
        </form>
        {!this.props.hello.loading && <div>{this.props.hello.message}</div>}
        {!!this.props.hello.loading && <div>Loading</div>}
      </div>
    )
  }
}

export default connect(
  state => ({
    hello: state.hello
  }),
  { fetchMessage }
)(Hello);

# Functional Programming Final Project

CSCE604123 - Functional Programming (Pemrograman Fungsional) @ Faculty of Computer Science Universitas Indonesia, Odd Semester 2019/2020

* * *

## Table of Contents

Welcome to Pemrograman Fungmeownal code repository.

1. [Quickstart Guide](#tldr)
2. [Initial Setup](#initial-setup)
3. [Car Price Calculation System](#calculation-system)
4. [Resources and References](#resources-and-references)

## TL;DR

This program will allow users to know which car to get based on their budget.

## Initial Setup

- This app uses [https://www.docker.com](Docker) and docker-compose.
- Just run `docker-compose up`

## Calculation System

This program will allow users to know which car to get based on their budget.

### Inputs :

1. Installment amount
2. User's salary
3. Maturity (in Month)
4. Seater size
6. Bank name where the user gets the loan

### Outputs :

1. Car name
2. Car specifications
3. Car price (loan amount)
4. New installment amount
5. Down-payment amount
6. Total repayment amount (load amount + interest amount)

### Calculation Steps

```
function getMaxLoanAmount(installment amount, maturity, interest rate) {
    money = installment amount * maturity
    maximum loan amount = money * (100-interest rate) / 100

    return maximum loan amount
}

function getCar(maximum loan amount, seater size) {

    for car in car lookup table {
        if car price less than or equal to maximum loan amount
            if car seater size equals to seater size
                then add car to list of cars
    }

    return the most expensive car in the list of cars
}

function getBankInfo(bank name) {

    Search bank name in column bank name from the lookup table

    return interest rate, down-payment rate
}

In the main function {

    Take installment amount, user's salary, maturity, seater size and bank name where the user gets the loan from user

    Send bank name to getBankInfo function and store the result in bankInfo variable

    Send installment amount, maturity and interest rate from bankInfo to getMaxLoanAmount function and store the result in maxLoanAmount variable

    Send maxLoanAmount and seater size to getCar function and store the result in choosenCar variable

    Down-payment amount = price of choosenCar * down-payment rate from bankInfo

    New installment amount = (price of choosenCar - Down-payment amount) / maturity

    Total repayment amount = price of choosenCar * (100 + interest rate from bankInfo) / 100

    Print car's name, specifications and price from choosenCar, Down-payment amount, New installment amount and Total repayment amount to the user
}
```

## Resources and References

Car prices:

- https://www.otosia.com/daftar-harga-mobil/
- https://kreditgogo.com/kredit-mobil/

User interface mockup:

[CicilMobilPas Mockup on Figma](https://www.figma.com/file/7pJzSLnDFhHd8OwqX2ltA2/CicilMobilPas-Functional-Programming-Final-Project?node-id=0%3A1)